package com.example.jwtspringsecurity.init;

import com.example.jwtspringsecurity.entity.Role;
import com.example.jwtspringsecurity.entity.User;
import com.example.jwtspringsecurity.repository.RoleRepo;
import com.example.jwtspringsecurity.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializer implements CommandLineRunner {
    private final RoleRepo roleRepo;
    private final UserService userService;

    public DataInitializer(RoleRepo roleRepo, UserService userService) {
        this.roleRepo = roleRepo;
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {
        List<Role> roleList = Arrays.asList(
                new Role("ADMIN"),
                new Role("TEACHER"),
                new Role("STUDENT")
        );

        /*roleRepo.saveAll(roleList);*/

        List<Role> roleList1 = Arrays.asList(roleRepo.findById(1L).get());
        List<Role> roleList2 = Arrays.asList(roleRepo.findById(2L).get());
        List<Role> roleList3 = Arrays.asList(roleRepo.findById(3L).get());

        List<User> userList = Arrays.asList(
                new User("tajbir", "12345", "Taj", "Bir", "taj@gmail.com", "016", roleList1),
                new User("nuva", "12345", "Tas", "Nuva", "nuva@gmail.com", "018", roleList2)
        );

        /*userService.saveAll(userList);*/


    }
}
