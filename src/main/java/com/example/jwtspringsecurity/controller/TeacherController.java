package com.example.jwtspringsecurity.controller;

import com.example.jwtspringsecurity.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @RequestMapping("/check")
    public String checkAuthenticate() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String roleType = "";
        for (GrantedAuthority role : user.getAuthorities()) {
            roleType += role.getAuthority();
        }
        return "Teacher is Authenticated" +
                "\n\nName:" + user.getFirstName() + " " + user.getLastName() +
                "\nUsername:" + user.getUsername() +
                "\nEmail:" + user.getEmail() +
                "\nMobile:" + user.getMobile() +
                "\n\nRole:" + roleType;
    }
}
