package com.example.jwtspringsecurity.controller;

import com.example.jwtspringsecurity.dto.ResponseDto;
import com.example.jwtspringsecurity.dto.UserDto;
import com.example.jwtspringsecurity.entity.User;
import com.example.jwtspringsecurity.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/signup")
@CrossOrigin
public class SignupController {
    private final UserService userService;

    public SignupController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/")
    public ResponseEntity<?> generateToken(@RequestBody UserDto userDto) {
        if (userService.isUserExist(userDto.getUsername())) {
            ResponseDto response = new ResponseDto("Username Already Used");
            return ResponseEntity.ok(response);
        } else {
            User user = new User();
            BeanUtils.copyProperties(userDto, user);
            userService.save(user);
            ResponseDto response = new ResponseDto("Save");
            return ResponseEntity.ok(response);
        }
    }
}
