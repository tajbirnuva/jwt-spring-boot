package com.example.jwtspringsecurity.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @RequestMapping("/")
    public String welcome() {
        return "Json Web Token (JWT)";
    }

    @RequestMapping("/check")
    public String checkAuthenticate() {
        return "User is Authenticated";
    }
}
